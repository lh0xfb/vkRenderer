# vkRenderer
  
## __Building:__

* mkdir build
* cd build
* cmake .. 
* cd ..
* cmake --build build --config Release

## __Running:__

* cd bin
* ./main <width> <height> 
