#pragma once

#include <cstdint>

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define NELEM(x) ( sizeof(x) / sizeof((x)[0]) )
#define Min(a, b) ((a) < (b) ? (a) : (b))
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Clamp(x, lo, hi) ( Min(hi, Max(lo, x)) )
#define Lerp(a, b, alpha) ( (1.0f - (alpha)) * (a) + (alpha) * (b) )