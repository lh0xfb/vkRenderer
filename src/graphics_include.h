#pragma once

#define _CRT_SECURE_NO_WARNINGS

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include "linmath.h"

#include "vulkan/vulkan.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ints.h"
#include "array.h"
#include "assert.h"

#define ENABLE_VK_VALIDATION 1
#define EMIT_TESTS 0

#if ENABLE_VK_VALIDATION
    #define VKDBG_ONLY(...) __VA_ARGS__
#else 
    #define VKDBG_ONLY(...) 
#endif // ENABLE_VK_VALIDATION
