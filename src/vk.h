#pragma once

#include "graphics_include.h"

namespace Vk
{    
    enum ShaderStage //: VkShaderStageFlagBits 
    {
        SS_Vertex = VK_SHADER_STAGE_VERTEX_BIT,
        SS_Fragment = VK_SHADER_STAGE_FRAGMENT_BIT,
        SS_Compute = VK_SHADER_STAGE_COMPUTE_BIT,
    };

    enum Topology //: VkPrimitiveTopology 
    {
        TOP_PointList = VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
        TOP_LineList = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
        TOP_LineStrip = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
        TOP_TriangleList = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        TOP_TriangleStrip = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
        TOP_TriangleFan = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
    };

    enum PolygonMode //: VkPolygonMode 
    {
        PM_Fill = VK_POLYGON_MODE_FILL,
        PM_Line = VK_POLYGON_MODE_LINE,
        PM_Point = VK_POLYGON_MODE_POINT,
    };

    enum FrontFace //: VkFrontFace 
    {
        FF_Clockwise = VK_FRONT_FACE_CLOCKWISE,
        FF_CounterClockwise = VK_FRONT_FACE_COUNTER_CLOCKWISE,
    };

    enum CullMode //: VkCullModeFlagBits 
    {
        CM_None = VK_CULL_MODE_NONE,
        CM_Front = VK_CULL_MODE_FRONT_BIT,
        CM_Back = VK_CULL_MODE_BACK_BIT,
        CM_FrontAndBack = VK_CULL_MODE_FRONT_AND_BACK,
    };

    enum MultisampleMode //: VkSampleCountFlagBits 
    {
        MSM_One = VK_SAMPLE_COUNT_1_BIT,
        MSM_Two = VK_SAMPLE_COUNT_2_BIT,
        MSM_Four = VK_SAMPLE_COUNT_4_BIT,
        MSM_Eight = VK_SAMPLE_COUNT_8_BIT,
    };

    enum AttachmentLoadOp //: VkAttachmentLoadOp 
    {
        ALO_Load = VK_ATTACHMENT_LOAD_OP_LOAD,
        ALO_Clear = VK_ATTACHMENT_LOAD_OP_CLEAR,
        ALO_DontCare = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
    };

    enum AttachmentStoreOp //: VkAttachmentStoreOp 
    {
        ASO_Store = VK_ATTACHMENT_STORE_OP_STORE,
        ASO_DontCare = VK_ATTACHMENT_STORE_OP_DONT_CARE,
    };

    enum ImageLayout //: VkImageLayout 
    {
        IL_Undefined = VK_IMAGE_LAYOUT_UNDEFINED,
        IL_General = VK_IMAGE_LAYOUT_GENERAL,
        IL_ColorAttachment = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        IL_DepthStencilAttachment = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        IL_DepthStencilRO = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL,
        IL_ShaderRO = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        IL_TransferSrc = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        IL_TransferDst = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        IL_PreInit = VK_IMAGE_LAYOUT_PREINITIALIZED,
        IL_PresentSrc = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        IL_SharedPresent = VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR,
    };

    struct QueueFamilyIndices
    {
        s32 m_graphicsFamily = -1;
        s32 m_presentFamily = -1;

        bool IsComplete() const 
        { 
            return m_graphicsFamily >= 0 && m_presentFamily >= 0;
        }
    };

    struct SwapChainSupportDetails
    {
        VkSurfaceCapabilitiesKHR    m_capabilities;
        Vector<VkSurfaceFormatKHR>  m_formats;
        Vector<VkPresentModeKHR>    m_presentModes;
    };

    struct Swapchain
    {
        VkSwapchainKHR m_handle;
        Vector<VkImage> m_images;
        Vector<VkImageView> m_views;
        Vector<VkFramebuffer> m_framebuffers;
        VkFormat m_format;
        VkExtent2D m_extent;
    };

    struct AttachmentOptions
    {
        VkFormat            m_format;
        MultisampleMode     m_samples;
        AttachmentLoadOp    m_loadOp;
        AttachmentStoreOp   m_storeOp;
        AttachmentLoadOp    m_stencilLoadOp;
        AttachmentStoreOp   m_stencilStoreOp;
        ImageLayout         m_initialLayout;
        ImageLayout         m_finalLayout;
    };

    struct SubpassOptions
    {
        VkPipelineBindPoint          m_bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        Slice<VkAttachmentReference> m_inputAttachments;
        Slice<VkAttachmentReference> m_colorAttachments;
        Slice<VkAttachmentReference> m_resolveAttachments;
        const VkAttachmentReference* m_depthAttachment = nullptr;
        Slice<u32>                   m_preserveAttachments;
    };

    struct GraphicsPipelineOptions
    {
        Topology        m_topology = TOP_TriangleList;
        PolygonMode     m_polygonMode = PM_Fill;
        FrontFace       m_frontFace = FF_Clockwise;
        CullMode        m_cullMode = CM_Back;
        MultisampleMode m_sampleMode = MSM_One;
        bool            m_colorEnabled = true;
        bool            m_alphaBlend = false;
    };

    struct Shader
    {
        VkShaderModule m_module;
        ShaderStage m_stage;
    };

    void GetInstanceLayerProperties(
        Vector<VkLayerProperties>& props);
    bool AllLayersPresent(
        Slice<VkLayerProperties> props, 
        Slice<const char*> layerNames);
    void GetPhysicalDeviceQueueFamilyProperties(
        VkPhysicalDevice device, 
        Vector<VkQueueFamilyProperties>& props);
    QueueFamilyIndices FindQueueFamilies(
        VkPhysicalDevice device, 
        VkSurfaceKHR surface = VK_NULL_HANDLE);
    void QuerySwapChainSupport(
        VkPhysicalDevice device, 
        VkSurfaceKHR surface, 
        SwapChainSupportDetails& details);
    VkInstance CreateInstance(
        const char* appName);
    void DestroyInstance(
        VkInstance instance);
    void SetupDebugCallback(
        VkInstance instance, 
        VkDebugReportCallbackEXT& reportExt);
    void ShutdownDebugCallback(
        VkInstance instance, 
        VkDebugReportCallbackEXT& reportExt);
    VkSurfaceKHR CreateSurface(
        VkInstance instance, 
        GLFWwindow* pWindow);
    void DestroySurface(
        VkInstance instance, 
        VkSurfaceKHR surface);
    bool CheckLogicalDeviceExtensionSupport(
        VkPhysicalDevice device, 
        Slice<const char*> extensions);
    float PhysicalDeviceSuitability(
        VkPhysicalDevice device, 
        VkSurfaceKHR surface, 
        Slice<const char*> logicalDeviceExtensions);
    VkSurfaceFormatKHR ChooseSwapSurfaceFormat(
        Slice<VkSurfaceFormatKHR> formats);
    VkPresentModeKHR ChooseSwapPresentMode(
        Slice<VkPresentModeKHR> modes);
    VkExtent2D ChooseSwapExtent(
        const VkSurfaceCapabilitiesKHR& capabilities, 
        u32 width, 
        u32 height);
    void GetPhysicalDevices(
        VkInstance instance, 
        Vector<VkPhysicalDevice>& devices);
    VkPhysicalDevice ChoosePhysicalDevice(
        VkInstance instance, 
        VkSurfaceKHR surface, 
        Slice<const char*> deviceExtensions);
    void GetRequiredLogicalDeviceExtensions(
        Vector<const char*>& extensions);
    void GetRequiredValidationLayerNames(
        Vector<const char*>& layers);
    void GetRequiredInstanceExtensions(
        Vector<const char*>& extensions);
    void CreateLogicalDevice(
        VkPhysicalDevice physicalDevice, 
        VkSurfaceKHR surface, 
        VkDevice& logicalDevice, 
        VkQueue& graphicsQueue, 
        VkQueue& presentQueue);
    void DestroyLogicalDevice(
        VkDevice logicalDevice);
    void CreateSwapChain(
        VkPhysicalDevice physicalDevice, 
        VkDevice logicalDevice, 
        VkSurfaceKHR surface, 
        u32 width, 
        u32 height, 
        Swapchain& swapchainObj);
    void DestroySwapchain(
        VkDevice logicalDevice, 
        Swapchain& rSwap);
    VkFramebuffer CreateFramebuffer(
        VkDevice device,
        VkRenderPass renderPass,
        Slice<VkImageView> attachments,
        u32 width,
        u32 height,
        u32 layers);
    void DestroyFramebuffer(
        VkDevice device,
        VkFramebuffer framebuffer);
    void CreateSwapchainFramebuffers(
        VkDevice device,
        VkRenderPass renderPass,
        Swapchain& rSwap);
    void InitGlfw();
    void ShutdownGlfw();
    GLFWwindow* CreateWindow(
        const char* name, 
        u32 width, 
        u32 height);
    void DestroyWindow(
        GLFWwindow* pWindow);
    void ReadFile(
        const char* filename, 
        Vector<u32>& buffer);
    Shader CreateShader(
        VkDevice logicalDevice, 
        Slice<u32> binary, 
        ShaderStage stage);
    void DestroyShader(
        VkDevice logicalDevice, 
        Shader shader);
    VkPipelineVertexInputStateCreateInfo CreateVertexInputInfo();
    VkPipelineInputAssemblyStateCreateInfo CreateInputAssemblyInfo(
        Topology topo);
    VkViewport CreateViewport(
        VkExtent2D extent);
    VkRect2D CreateScissor(
        s32 x_offset, 
        s32 y_offset, 
        VkExtent2D extent);
    VkPipelineViewportStateCreateInfo CreateViewportStateInfo(
        const VkViewport& viewport, 
        const VkRect2D& scissor);
    VkPipelineRasterizationStateCreateInfo CreateRasterizationInfo(
        PolygonMode polyMode,
        FrontFace frontFace,
        CullMode cullMode);
    VkPipelineMultisampleStateCreateInfo CreateMultisampleInfo(
        MultisampleMode mode);
    VkPipelineColorBlendAttachmentState CreateColorBlendAttachmentInfo(
        bool color, 
        bool blendEnable);
    VkPipelineColorBlendStateCreateInfo CreateColorBlendStateInfo(
        const VkPipelineColorBlendAttachmentState& colorBlendAttachment);
    VkPipelineLayout CreatePipelineLayout(
        VkDevice device,
        Slice<VkDescriptorSetLayout> setLayouts = {},
        Slice<VkPushConstantRange> pushConstantRanges = {});
    void DestroyPipelineLayout(
        VkDevice device, 
        VkPipelineLayout layout);
    VkAttachmentDescription CreateAttachmentDescription(
        const AttachmentOptions& options);
    VkAttachmentReference CreateAttachmentReference(
        VkImageLayout layout, 
        u32 attachment);
    VkSubpassDescription CreateSubpassDescription(
        const SubpassOptions& options);
    VkRenderPass CreateRenderPass(
        VkDevice device,
        Slice<VkAttachmentDescription> attachments,
        Slice<VkSubpassDescription> subpasses,
        Slice<VkSubpassDependency> dependencies = {});
    void DestroyRenderPass(
        VkDevice device, 
        VkRenderPass renderPass);
    VkPipeline CreateGraphicsPipeline(
        VkDevice logicalDevice, 
        Slice<Shader> shaders,
        const VkPipelineLayout& pipelineLayout,
        VkRenderPass renderPass,
        u32 subpassIdx,
        const GraphicsPipelineOptions& options,
        VkExtent2D extent);
    void DestroyGraphicsPipeline(
        VkDevice device,
        VkPipeline pipeline);
    VkCommandPool CreateCommandPool(
        VkPhysicalDevice physicalDevice,
        VkDevice logicalDevice,
        u32 flags = 0);
    void DestroyCommandPool(
        VkDevice device,
        VkCommandPool commandPool);
    void AllocateCommandBuffers(
        VkDevice device,
        VkCommandPool commandPool,
        Vector<VkCommandBuffer>& buffers,
        bool indirect = false);
    void BeginCommandBuffer(
        VkCommandBuffer buffer);
    void BeginRenderPass(
        VkRenderPass renderPass,
        VkFramebuffer framebuffer,
        VkCommandBuffer cmdBuffer,
        VkExtent2D extent,
        VkClearValue clearColor,
        bool indirect = false);
    void CmdBindGraphicsPipeline(
        VkCommandBuffer cmdBuffer,
        VkPipeline pipeline);
    void CmdDraw(
        VkCommandBuffer cmdBuffer,
        u32 vertCount,
        u32 instanceCount = 1,
        u32 firstVertex = 0,
        u32 firstInstance = 0);
    void CmdEndRenderPass(
        VkCommandBuffer cmdBuffer);
    void EndCommandBuffer(
        VkCommandBuffer cmdBuffer);
};
