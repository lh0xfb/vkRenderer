#pragma once

#include <atomic>
#include <cassert>
#include "ints.h"

// Only safe for 1 consumer 1 producer situations
template<typename T, u32 capacity>
class CircularQueue
{
    T data[capacity];
    std::atomic<u32> read_idx, write_idx;

    u32 mask(u32 value) const
    {
        return value & (capacity - 1);
    }
public:
    CircularQueue() : read_idx(0u), write_idx(0u)
    {
        assert(mask(capacity) == 0u);
    }
    bool empty() const
    {
        u32 r = read_idx;
        u32 w = write_idx;
        return r == w;
    }
    bool full() const
    {
        u32 r = read_idx;
        u32 w = write_idx;
        return r + 1u == w;
    }
    u32 count()
    {
        u32 r = read_idx;
        u32 w = write_idx;
        if(r > w)
        {
            // rolled around over 0xffffffff, rare but possible
            r += capacity;
            w += capacity;
        }
        return w - r;
    }
    void push(const T& item)
    {
        u32 idx = write_idx++;
        idx = mask(idx);
        data[idx] = item;
    }
    T pop()
    {
        u32 idx = read_idx++;
        idx = mask(idx);
        return data[idx];
    }
    void clear()
    {
        read_idx = 0u;
        write_idx = 0u;
    }
};
