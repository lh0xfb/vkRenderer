#pragma once

#include "ints.h"

template<typename T>
class Slice
{
    T* m_begin;
    T* m_end;
public:
    Slice() : m_begin(nullptr), m_end(nullptr);
    Slice(T* pBegin, T* pEnd) : m_begin(pBegin), m_end(pEnd){}
    Slice(T* pBegin, size_t count) : m_begin(pBegin), m_end(pBegin + count){}
    T* begin() const { return m_begin; }
    T* end() const { return m_end; }
    T& operator[](size_t idx) const { return m_begin[idx]; }
    size_t count() const { return size_t(m_end - m_begin); }
};

template<typename T>
class CSlice
{
    const T* m_begin;
    const T* m_end;
public:
    CSlice() : m_begin(nullptr), m_end(nullptr);
    CSlice(const T* pBegin, const T* pEnd) : m_begin(pBegin), m_end(pEnd){}
    CSlice(const T* pBegin, size_t count) : m_begin(pBegin), m_end(pBegin + count){}
    const T* begin() const { return m_begin; }
    const T* end() const { return m_end; }
    const T& operator[](size_t idx) const { return m_begin[idx]; }
    size_t count() const { return size_t(m_end - m_begin); }
};

#define Slc(x) { (x), NELEM(x) }