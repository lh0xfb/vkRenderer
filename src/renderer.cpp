#include "renderer.h"

#include "graphics_include.h"

void Renderer::Init()
{
    Vk::InitGlfw();
    Vector<const char*> deviceExtensions;
    Vk::GetRequiredLogicalDeviceExtensions(deviceExtensions);

    m_pWindow = Vk::CreateWindow("Renderer", m_width, m_height);
    m_instance = Vk::CreateInstance("Renderer");
    Vk::SetupDebugCallback(m_instance, m_reportExtension);
    m_surface = Vk::CreateSurface(m_instance, m_pWindow);
    m_physicalDevice = Vk::ChoosePhysicalDevice(m_instance, m_surface, deviceExtensions);
    Vk::CreateLogicalDevice(m_physicalDevice, m_surface, m_logicalDevice, m_graphicsQueue, m_presentQueue);
    Vk::CreateSwapChain(m_physicalDevice, m_logicalDevice, m_surface, m_width, m_height, m_swapchain);
    
    Vector<Vk::Shader> shaders;
    Vector<u32> vertCode, fragCode;
    Vk::ReadFile("shader_bin/main.vert.spv", vertCode);
    shaders.grow() = Vk::CreateShader(m_logicalDevice, vertCode, Vk::SS_Vertex);
    Vk::ReadFile("shader_bin/main.frag.spv", fragCode);
    shaders.grow() = Vk::CreateShader(m_logicalDevice, fragCode, Vk::SS_Fragment);

    m_pipelineLayout = Vk::CreatePipelineLayout(m_logicalDevice);

    Vector<VkAttachmentDescription> attachmentDescs;
    {
        attachmentDescs.grow() = Vk::CreateAttachmentDescription(
            {
                m_swapchain.m_format,
                Vk::MSM_One,
                Vk::ALO_Clear,
                Vk::ASO_Store,
                Vk::ALO_DontCare,
                Vk::ASO_DontCare,
                Vk::IL_ColorAttachment,
                Vk::IL_PresentSrc
            }
        );
    }
    Vector<VkSubpassDescription> subpasses;
    Vk::SubpassOptions spOptions = {};
    Vector<VkAttachmentReference> colorAttachments;
    colorAttachments.grow() = Vk::CreateAttachmentReference(
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        0);
    spOptions.m_colorAttachments = colorAttachments;
    subpasses.grow() = Vk::CreateSubpassDescription(spOptions);

    m_renderPass = Vk::CreateRenderPass(
        m_logicalDevice,
        attachmentDescs,
        subpasses
    );
    
    m_pipeline = Vk::CreateGraphicsPipeline(
        m_logicalDevice, 
        shaders, 
        m_pipelineLayout,
        m_renderPass,
        0,
        Vk::GraphicsPipelineOptions(),
        m_swapchain.m_extent
    );

    for(const Vk::Shader& shader : shaders)
    {
        Vk::DestroyShader(m_logicalDevice, shader);
    }

    Vk::CreateSwapchainFramebuffers(m_logicalDevice, m_renderPass, m_swapchain);
    m_commandPool = Vk::CreateCommandPool(m_physicalDevice, m_logicalDevice);

    m_commandBuffers.resize(m_swapchain.m_framebuffers.count());
    Vk::AllocateCommandBuffers(m_logicalDevice, m_commandPool, m_commandBuffers);

    for(s32 i = 0; i < m_commandBuffers.count(); ++i)
    {
        const VkCommandBuffer& cmdBuf = m_commandBuffers[i];
        Vk::BeginCommandBuffer(cmdBuf);

        Vk::BeginRenderPass(
            m_renderPass, 
            m_swapchain.m_framebuffers[i], 
            cmdBuf,
            m_swapchain.m_extent,
            {0.0f, 0.0f, 0.0f, 1.0f});
        {
            Vk::CmdBindGraphicsPipeline(cmdBuf, m_pipeline);
            Vk::CmdDraw(cmdBuf, 3);
        }
        Vk::CmdEndRenderPass(cmdBuf);
        Vk::EndCommandBuffer(cmdBuf);
    }
}

void Renderer::Loop()
{
    while(!glfwWindowShouldClose(m_pWindow)) 
    {
        glfwPollEvents();
    }
}

void Renderer::Shutdown()
{
    Vk::DestroyCommandPool(m_logicalDevice, m_commandPool);
    Vk::DestroyGraphicsPipeline(m_logicalDevice, m_pipeline);
    Vk::DestroyPipelineLayout(m_logicalDevice, m_pipelineLayout);
    Vk::DestroyRenderPass(m_logicalDevice, m_renderPass);
    Vk::DestroySwapchain(m_logicalDevice, m_swapchain);
    Vk::DestroyLogicalDevice(m_logicalDevice);
    Vk::DestroySurface(m_instance, m_surface);
    Vk::ShutdownDebugCallback(m_instance, m_reportExtension);
    Vk::DestroyInstance(m_instance);
    Vk::DestroyWindow(m_pWindow);
    Vk::ShutdownGlfw();
}
