#pragma once 

#include "ints.h"

inline u32 fnv(const char* name)
{
    u32 val = 2166136261u;
    for(const u8* data = (const u8*)name; *data; ++data)
    {
        val ^= *data;
        val *= 16777619u;
    }
    val |= val==0;
    return val;
}

inline u32 fnv(const void* p, const u32 len)
{
    const u8* data = (const u8*)p;
    u32 val = 2166136261u;
    for(u32 i = 0; i < len; i++)
    {
        val ^= data[i];
        val *= 16777619u;
    }
    val |= val==0;
    return val;
}
