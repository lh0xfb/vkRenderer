#pragma once

#include "ints.h"

struct GLFWwindow;

namespace RI
{
    void Run();
    void SetWidth(u32 width);
    void SetHeight(u32 height);
    u32 GetWidth();
    u32 GetHeight();
    GLFWwindow* GetWindow();
};
