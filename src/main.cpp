#include "render_interface.h"

#include <cstdlib>

int main(int argc, const char** argv) 
{
    if(argc > 1)
    {
        RI::SetWidth(atoi(argv[1]));
    }
    if(argc > 2)
    {
        RI::SetHeight(atoi(argv[2]));
    }

    RI::Run();

    return 0;
}