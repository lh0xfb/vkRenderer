#pragma once

#include "vk.h"

class Renderer
{
    GLFWwindow*                 m_pWindow = nullptr;
    u32                         m_width = 800;
    u32                         m_height = 600;
    VkInstance                  m_instance;
    VkSurfaceKHR                m_surface;
    VkPhysicalDevice            m_physicalDevice;
    VkDevice                    m_logicalDevice;
    VkQueue                     m_graphicsQueue;
    VkQueue                     m_presentQueue;
    Vk::Swapchain               m_swapchain;
    VkPipelineLayout            m_pipelineLayout;
    VkPipeline                  m_pipeline;
    VkRenderPass                m_renderPass;
    VkCommandPool               m_commandPool;
    Vector<VkCommandBuffer>     m_commandBuffers;
    VkDebugReportCallbackEXT    m_reportExtension;

public:
    void Init();
    void Loop();
    void Shutdown();

    void SetWidth(u32 width) { m_width = width; }
    void SetHeight(u32 height) { m_height = height; }
    u32 GetWidth() const { return m_width; }
    u32 GetHeight() const { return m_height; }
    GLFWwindow* GetWindow() const { return m_pWindow; }
};

