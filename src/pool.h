#pragma once

#include "ints.h"
#include "array.h"
#include <cassert>

template<typename T, u16 t_capacity>
class Pool
{
    T   m_data[t_capacity];
    u16 m_next[t_capacity];
    u32 m_allocated[t_capacity / 32u + 1u];
    u16 m_head;
    u16 m_tail;
    u16 m_count;


    u32& GetAllocation(u16 idx, u32& mask) 
    {
        const u16 slot = idx / 32u;
        const u16 bit = idx % 32u;
        mask = 1u << (u32)bit;
        return m_allocated[slot];
    }
public:
    u16 SlotsAvailable() const { return t_capacity - m_count - 1u; }
    bool Full() const { return SlotsAvailable() < 1u; }
    bool Empty() const { return m_count == 0u;}
    void Init()
    {
        for(T& t : m_data)
        {
            t = T();
        }
        for(u16 i = 0u; i < t_capacity; ++i)
        {
            m_next[i] = i + 1u;
        }
        for(u16 i = 0u; i < t_capacity / 32u + 1; ++i)
        {
            m_allocated[i] = 0u;
        }
        m_head = 0u;
        m_tail = t_capacity - 1u;
        m_count = 0u;
    }
    T* Allocate()
    {
        assert(!Full());
        const u16 slot = m_head;
        m_head = m_next[slot];
        u32 mask = 0u;
        u32& allocation = GetAllocation(slot, mask);
        assert((allocation & mask) == 0u);
        allocation |= mask;
        ++m_count;
        return m_data + slot;
    }
    void Free(T* item)
    {
        assert(!Empty());
        const ptrdiff_t slot = item - m_data;
        assert(slot < (size_t)t_capacity);
        const u16 slotIdx = (u16)slot;
        u32 mask = 0u;
        u32& allocation = GetAllocation(slotIdx, mask);
        assert((allocation & mask) != 0u);
        allocation &= ~mask;
        --m_count;

        const u16 head = m_head;
        const u16 tail = m_tail;

        u16 idx = head;
        u16 prev = tail;
        while(slotIdx > idx && idx != tail)
        {
            prev = idx;
            idx = m_next[idx];
        }

        if(idx == head)
        {
            m_head = slotIdx;
            m_next[slotIdx] = idx;
        }
        else if(idx == tail)
        {
            m_next[tail] = slotIdx;
            m_tail = slotIdx;
        }
        else
        {
            m_next[prev] = slotIdx;
            m_next[slotIdx] = idx;
        }
    }
};

#if EMIT_TESTS

#include <cstdio>
#include <ctime>
#include "randf.h"

inline void PoolTest()
{
    g_randSeed = (u32)time(nullptr);
    Pool<u32, 256> p;
    Array<u32*, 256> allocs;
    p.Init();

    for(u32 i = 0; i < 10000; ++i)
    {
        const float prob = 0.5f;
        for(u32 j = 0; j < 10000; ++j)
        {
            const bool add = randf() < prob;
            const bool full = p.Full();
            const bool empty = p.Empty();
            if((add && !full) || empty)
            {
                u32* x = p.Allocate();
                assert(allocs.find(x) == -1);
                allocs.grow() = x;
            }
            else
            {
                const s32 idx = rand(g_randSeed) % allocs.count();
                p.Free(allocs[idx]);
                allocs.remove(idx);
            }
        }
    }
}

#endif