#pragma once

#include "circular_queue.h"
#include "string.h"
#include <thread>
#include <chrono>

typedef void (*TaskFn)(void*);

struct Task
{
    TaskFn  m_function;
    TaskFn  m_destructor;
    u8      m_data[64];

    void call()
    {
        m_function((void*)m_data);
        m_destructor((void*)m_data);
    }
};

class TaskManager
{
    CircularQueue<Task, 64> m_tasks;
    std::thread m_thread;
    volatile bool m_running;
public:
    TaskManager()
    {
        m_running = true;
        m_thread = std::thread(&TaskManager::ThreadUpdate, this);
    }
    ~TaskManager()
    {
        m_running = false;
        m_thread.join();
    }
    void ThreadUpdate()
    {
        while(m_running)
        {
            while(!m_tasks.empty())
            {
                Task task = m_tasks.pop();
                task.call();
            }
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1ms);
        }
    }
    bool Full() const 
    {
        return m_tasks.full();
    }
    bool Empty() const 
    {
        return m_tasks.empty();
    }
    template<typename T>
    void CreateTask(const T& data)
    {
        Task task;
        assert(sizeof(T) <= sizeof(task.m_data));
        task.m_function = [](void* pVoid)
        {
            T& item = *(T*)pVoid;
            item();
        };
        task.m_destructor = [](void* pVoid)
        {
            T* pItem = (T*)pVoid;
            pItem->~T();
        };
        memset(task.m_data, 0, sizeof(task.m_data));
        T* pT = (T*)task.m_data;
        new (pT) T();
        *pT = data;
        assert(!m_tasks.full());
        m_tasks.push(task);
    }
};

extern TaskManager g_TaskManager;