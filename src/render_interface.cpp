#include "render_interface.h"

#include "renderer.h"

Renderer g_renderer;

namespace RI
{
    void Run()
    {
        g_renderer.Init();
        g_renderer.Loop();
        g_renderer.Shutdown();
    }
    void SetWidth(u32 width)
    {
        g_renderer.SetWidth(width);
    }
    void SetHeight(u32 height)
    {
        g_renderer.SetHeight(height);
    }
    u32 GetWidth()
    {
        return g_renderer.GetWidth();
    }
    u32 GetHeight()
    {
        return g_renderer.GetHeight();
    }
    GLFWwindow* GetWindow()
    {
        return g_renderer.GetWindow();
    }
};