#include "vk.h"

VKDBG_ONLY(
    static VKAPI_ATTR VkBool32 VKAPI_CALL PutsVulkanMsg(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objType,
        uint64_t obj,
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* msg,
        void* userData) 
    {
        puts(msg);
        return VK_FALSE;
    }
);

void Vk::GetInstanceLayerProperties(
    Vector<VkLayerProperties>& props)
{
    u32 count = 0;
    vkEnumerateInstanceLayerProperties(&count, nullptr);
    props.resize(count);
    vkEnumerateInstanceLayerProperties(&count, props.begin());
}

bool Vk::AllLayersPresent(
    Slice<VkLayerProperties> props, 
    Slice<const char*> layerNames)
{
    for(const char* layer : layerNames)
    {
        bool found = false;
        for(const VkLayerProperties& properties : props)
        {
            if(strcmp(layer, properties.layerName) == 0)
            {
                found = true;
                break;
            }
        }
        if(!found)
        {
            return false;
        }
    }
    return true;
}

void Vk::GetPhysicalDeviceQueueFamilyProperties(
    VkPhysicalDevice device, 
    Vector<VkQueueFamilyProperties>& props)
{
    u32 count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
    props.resize(count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, props.begin());
}

Vk::QueueFamilyIndices Vk::FindQueueFamilies(
    VkPhysicalDevice device, 
    VkSurfaceKHR surface)
{
    QueueFamilyIndices indices;
    Vector<VkQueueFamilyProperties> properties;
    GetPhysicalDeviceQueueFamilyProperties(device, properties);
    
    s32 i = 0;
    for(const VkQueueFamilyProperties& prop : properties)
    {
        if(prop.queueCount < 1)
        {
            continue;
        }
        if(prop.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices.m_graphicsFamily = i;
        }
        if(surface != VK_NULL_HANDLE)
        {
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
            if(presentSupport)
            {
                indices.m_presentFamily = i;
            }
            if(indices.IsComplete())
            {
                break;
            }
        }
        else if(indices.m_graphicsFamily != -1)
        {
            break;
        }
        ++i;
    }
    return indices;
}

void Vk::QuerySwapChainSupport(
    VkPhysicalDevice device, 
    VkSurfaceKHR surface, 
    SwapChainSupportDetails& details)
{
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.m_capabilities);

    u32 formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
    if(formatCount > 0)
    {
        details.m_formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.m_formats.begin());
    }

    u32 presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

    if (presentModeCount > 0)
    {
        details.m_presentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.m_presentModes.begin());
    }
}

VkInstance Vk::CreateInstance(
    const char* appName)
{
    VkInstance instance = {};

    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = appName;
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledLayerCount = 0;

    Vector<const char*> requiredInstanceLayers;
    Vector<const char*> requiredInstanceExtensions;

    GetRequiredValidationLayerNames(requiredInstanceLayers);
    GetRequiredInstanceExtensions(requiredInstanceExtensions);    
    
    {
        Vector<VkLayerProperties> layerProperties;
        GetInstanceLayerProperties(layerProperties);
        assert(AllLayersPresent(layerProperties, requiredInstanceLayers));
    }

    createInfo.enabledExtensionCount = requiredInstanceExtensions.count();
    createInfo.ppEnabledExtensionNames = requiredInstanceExtensions.begin();
    createInfo.enabledLayerCount = requiredInstanceLayers.count();
    createInfo.ppEnabledLayerNames = requiredInstanceLayers.begin();

    assert(vkCreateInstance(&createInfo, nullptr, &instance) == VK_SUCCESS);

    return instance;
}

void Vk::DestroyInstance(
    VkInstance instance)
{
    vkDestroyInstance(instance, nullptr);
}

void Vk::SetupDebugCallback(
    VkInstance instance, 
    VkDebugReportCallbackEXT& reportExt)
{
    VKDBG_ONLY(
        VkDebugReportCallbackCreateInfoEXT cbInfo = {};
        cbInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        cbInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        cbInfo.pfnCallback = PutsVulkanMsg;

        auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
        assert(func != nullptr);
        assert(func(instance, &cbInfo, nullptr, &reportExt) == VK_SUCCESS);
    );
}

void Vk::ShutdownDebugCallback(
    VkInstance instance, 
    VkDebugReportCallbackEXT& reportExt)
{
    VKDBG_ONLY(
        auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
        assert(func != nullptr);
        func(instance, reportExt, nullptr);
    );
}

VkSurfaceKHR Vk::CreateSurface(
    VkInstance instance, 
    GLFWwindow* pWindow)
{
    VkSurfaceKHR surface;
    assert(glfwCreateWindowSurface(instance, pWindow, nullptr, &surface) == VK_SUCCESS);
    return surface;
}

void Vk::DestroySurface(
    VkInstance instance, 
    VkSurfaceKHR surface)
{
    vkDestroySurfaceKHR(instance, surface, nullptr);
}

bool Vk::CheckLogicalDeviceExtensionSupport(
    VkPhysicalDevice device, 
    Slice<const char*> extensions)
{
    u32 count = 0;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);
    Vector<VkExtensionProperties> available;
    available.resize(count);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &count, available.begin());

    for(const char* reqd : extensions)
    {
        bool found = false;
        for(const VkExtensionProperties& prop : available)
        {
            if(strcmp(reqd, prop.extensionName) == 0)
            {
                found = true;
                break;
            }
        }
        if(!found)
        {
            return false;
        }
    }

    return true;
}

float Vk::PhysicalDeviceSuitability(
    VkPhysicalDevice device, 
    VkSurfaceKHR surface, 
    Slice<const char*> logicalDeviceExtensions)
{
    float suitability = 1.0f;

    VkPhysicalDeviceProperties deviceProperties;
    VkPhysicalDeviceFeatures deviceFeatures;
    vkGetPhysicalDeviceProperties(device, &deviceProperties);
    vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    if(deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
    {
        suitability *= 2.0f;
    }

    QueueFamilyIndices indices = FindQueueFamilies(device, surface);
    if(indices.IsComplete())
    {
        suitability *= 2.0f;
    }
    
    bool extensionsSupported = CheckLogicalDeviceExtensionSupport(device, logicalDeviceExtensions);
    if(extensionsSupported)
    {
        suitability *= 2.0f;
    }

    SwapChainSupportDetails swapChainDetails;
    QuerySwapChainSupport(device, surface, swapChainDetails);
    bool swapChainAdequate = swapChainDetails.m_formats.count() > 0 && swapChainDetails.m_presentModes.count() > 0;
    if(swapChainAdequate)
    {
        suitability *= 2.0f;
    }

    return suitability;
}

VkSurfaceFormatKHR Vk::ChooseSwapSurfaceFormat(
    Slice<VkSurfaceFormatKHR> formats)
{
    assert(formats.count() > 0u);

    const auto preferredFormat = VK_FORMAT_B8G8R8A8_UNORM;
    const auto preferredColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

    if(formats.count() == 1u && formats[0].format == VK_FORMAT_UNDEFINED)
    {
        return { preferredFormat, preferredColorSpace };
    }

    for(const VkSurfaceFormatKHR& format : formats)
    {
        if(format.format == preferredFormat &&
            format.colorSpace == preferredColorSpace)
        {
            return format;
        }
    }

    return formats[0];
}

VkPresentModeKHR Vk::ChooseSwapPresentMode(
    Slice<VkPresentModeKHR> modes)
{
    assert(modes.count() > 0);
    const VkPresentModeKHR choices[] = 
    {
        VK_PRESENT_MODE_FIFO_KHR,
        VK_PRESENT_MODE_MAILBOX_KHR,
        VK_PRESENT_MODE_IMMEDIATE_KHR
    };
    for(VkPresentModeKHR choice : choices)
    {
        for(VkPresentModeKHR mode : modes)
        {
            if(mode == choice)
            {
                return mode;
            }
        }
    }
    return modes[0];
}

VkExtent2D Vk::ChooseSwapExtent(
    const VkSurfaceCapabilitiesKHR& capabilities, 
    u32 width, 
    u32 height)
{
    if(capabilities.currentExtent.width != 0xffffffff)
    {
        return capabilities.currentExtent;
    }
    VkExtent2D actualExtent = { width, height};
    actualExtent.width = Max(capabilities.minImageExtent.width, Min(capabilities.maxImageExtent.width, width));
    actualExtent.height = Max(capabilities.minImageExtent.height, Min(capabilities.maxImageExtent.height, height));
    return actualExtent;
}

void Vk::GetPhysicalDevices(
    VkInstance instance, 
    Vector<VkPhysicalDevice>& devices)
{
    u32 count = 0;
    vkEnumeratePhysicalDevices(instance, &count, nullptr);
    devices.resize(count);
    vkEnumeratePhysicalDevices(instance, &count, devices.begin());
}

VkPhysicalDevice Vk::ChoosePhysicalDevice(
    VkInstance instance, 
    VkSurfaceKHR surface, 
    Slice<const char*> deviceExtensions)
{
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    Vector<VkPhysicalDevice> devices;
    GetPhysicalDevices(instance, devices);

    float suitability = 0.0f;
    for(VkPhysicalDevice device : devices)
    {
        float choice = PhysicalDeviceSuitability(device, surface, deviceExtensions);
        if(choice > suitability)
        {
            physicalDevice = device;
            suitability = choice;
        }
    }

    return physicalDevice;
}

void Vk::GetRequiredLogicalDeviceExtensions(
    Vector<const char*>& extensions)
{
    extensions.clear();
    extensions.grow() = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
}

void Vk::GetRequiredValidationLayerNames(
    Vector<const char*>& layers)
{
    layers.clear();
    VKDBG_ONLY(layers.grow() = "VK_LAYER_LUNARG_standard_validation");
}

void Vk::GetRequiredInstanceExtensions(
    Vector<const char*>& extensions)
{
    extensions.clear();
    VKDBG_ONLY(extensions.grow() = VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    {
        u32 glfwExtensionCount = 0;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
        for(u32 i = 0; i < glfwExtensionCount; ++i)
        {
            extensions.grow() = glfwExtensions[i];
        }
    }
}

void Vk::CreateLogicalDevice(
    VkPhysicalDevice physicalDevice, 
    VkSurfaceKHR surface, 
    VkDevice& logicalDevice, 
    VkQueue& graphicsQueue, 
    VkQueue& presentQueue)
{
    Vector<const char*> requiredLogicalDeviceExtensions;
    Vector<const char*> requiredValidationLayerNames;
    GetRequiredLogicalDeviceExtensions(requiredLogicalDeviceExtensions);
    GetRequiredValidationLayerNames(requiredValidationLayerNames);

    QueueFamilyIndices indices = FindQueueFamilies(physicalDevice, surface);

    Array<VkDeviceQueueCreateInfo, 2> queueCreateInfos;
    Array<s32, 2> families;
    families.grow() = indices.m_graphicsFamily;
    if(families.find(indices.m_presentFamily) == -1)
    {
        families.grow() = indices.m_presentFamily;
    }

    for(s32 family : families)
    {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = family;
        queueCreateInfo.queueCount = 1;
        float queuePriority = 1.0f;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.grow() = queueCreateInfo;
    }

    VkPhysicalDeviceFeatures deviceFeatures = {};

    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfos.begin();
    createInfo.queueCreateInfoCount = queueCreateInfos.count();

    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = requiredLogicalDeviceExtensions.count();
    createInfo.ppEnabledExtensionNames = requiredLogicalDeviceExtensions.begin();

    createInfo.enabledLayerCount = 0;

    createInfo.enabledLayerCount = requiredValidationLayerNames.count();
    createInfo.ppEnabledLayerNames = requiredValidationLayerNames.begin();

    assert(vkCreateDevice(physicalDevice, &createInfo, nullptr, &logicalDevice) == VK_SUCCESS);

    vkGetDeviceQueue(logicalDevice, indices.m_graphicsFamily, 0, &graphicsQueue);
    assert(graphicsQueue != VK_NULL_HANDLE);
    vkGetDeviceQueue(logicalDevice, indices.m_presentFamily, 0, &presentQueue);
    assert(presentQueue != VK_NULL_HANDLE);
}

void Vk::DestroyLogicalDevice(
    VkDevice logicalDevice)
{
    vkDestroyDevice(logicalDevice, nullptr);
}

void Vk::CreateSwapChain(
    VkPhysicalDevice physicalDevice, 
    VkDevice logicalDevice, 
    VkSurfaceKHR surface, 
    u32 width, 
    u32 height, 
    Swapchain& rSwap)
{
    rSwap.m_handle = VK_NULL_HANDLE;

    SwapChainSupportDetails swapChainSupport;
    QuerySwapChainSupport(physicalDevice, surface, swapChainSupport);

    const VkSurfaceFormatKHR surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.m_formats);
    const VkPresentModeKHR presentMode = ChooseSwapPresentMode(swapChainSupport.m_presentModes);
    const VkExtent2D extent = ChooseSwapExtent(swapChainSupport.m_capabilities, width, height);

    u32 imageCount = swapChainSupport.m_capabilities.minImageCount + 1;
    const u32 maxImageCount = swapChainSupport.m_capabilities.maxImageCount;
    if(maxImageCount > 0 && imageCount > maxImageCount)
    {
        imageCount = maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;

    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    QueueFamilyIndices indices = FindQueueFamilies(physicalDevice, surface);
    const u32 queueFamilyIndices[] = 
    {
        (u32)indices.m_graphicsFamily,
        (u32)indices.m_presentFamily
    };

    if(queueFamilyIndices[0] != queueFamilyIndices[1])
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = nullptr;
    }

    createInfo.preTransform = swapChainSupport.m_capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    assert(vkCreateSwapchainKHR(logicalDevice, &createInfo, nullptr, &rSwap.m_handle) == VK_SUCCESS);

    vkGetSwapchainImagesKHR(logicalDevice, rSwap.m_handle, &imageCount, nullptr);
    rSwap.m_images.resize(imageCount);
    vkGetSwapchainImagesKHR(logicalDevice, rSwap.m_handle, &imageCount, rSwap.m_images.begin());

    rSwap.m_format = surfaceFormat.format;
    rSwap.m_extent = extent;

    rSwap.m_views.resize(rSwap.m_images.count());
    for(s32 i = 0; i < rSwap.m_images.count(); ++i)
    {
        VkImageViewCreateInfo viewCreateInfo = {};
        viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewCreateInfo.image = rSwap.m_images[i];
        viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewCreateInfo.format = rSwap.m_format;

        viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

        viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewCreateInfo.subresourceRange.baseMipLevel = 0;
        viewCreateInfo.subresourceRange.levelCount = 1;
        viewCreateInfo.subresourceRange.baseArrayLayer = 0;
        viewCreateInfo.subresourceRange.layerCount = 1;

        assert(vkCreateImageView(logicalDevice, &viewCreateInfo, nullptr, &rSwap.m_views[i]) == VK_SUCCESS);
    }
}

void Vk::DestroySwapchain(
    VkDevice logicalDevice, 
    Swapchain& rSwap)
{
    for(VkImageView iview : rSwap.m_views)
    {
        vkDestroyImageView(logicalDevice, iview, nullptr);
    }
    rSwap.m_views.clear();
    rSwap.m_images.clear();
    for(VkFramebuffer fbuf : rSwap.m_framebuffers)
    {
        Vk::DestroyFramebuffer(logicalDevice, fbuf);
    }
    rSwap.m_framebuffers.clear();
    vkDestroySwapchainKHR(logicalDevice, rSwap.m_handle, nullptr);
}

VkFramebuffer Vk::CreateFramebuffer(
    VkDevice device,
    VkRenderPass renderPass,
    Slice<VkImageView> attachments,
    u32 width,
    u32 height,
    u32 layers)
{
    VkFramebuffer framebuffer = VK_NULL_HANDLE;

    VkFramebufferCreateInfo framebufferInfo = {};
    framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass = renderPass;
    framebufferInfo.attachmentCount = (u32)attachments.count();
    framebufferInfo.pAttachments = attachments.begin();
    framebufferInfo.width = width;
    framebufferInfo.height = height;
    framebufferInfo.layers = layers;

    assert(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &framebuffer) == VK_SUCCESS);
    
    return framebuffer;
}

void Vk::DestroyFramebuffer(
    VkDevice device,
    VkFramebuffer framebuffer)
{
    vkDestroyFramebuffer(device, framebuffer, nullptr);
}

void Vk::CreateSwapchainFramebuffers(
    VkDevice device,
    VkRenderPass renderPass,
    Swapchain& rSwap)
{
    rSwap.m_framebuffers.resize(rSwap.m_images.count());
    for(s32 i = 0; i < rSwap.m_images.count(); ++i)
    {
        rSwap.m_framebuffers[i] = Vk::CreateFramebuffer(
            device, 
            renderPass,
            { rSwap.m_views[i] },
            rSwap.m_extent.width,
            rSwap.m_extent.height,
            1);
    }
}

void Vk::InitGlfw()
{
    assert(glfwInit() == GLFW_TRUE);
}

void Vk::ShutdownGlfw()
{
    glfwTerminate();
}

GLFWwindow* Vk::CreateWindow(
    const char* name, 
    u32 width, 
    u32 height)
{
    GLFWwindow* pWindow = nullptr;
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    pWindow = glfwCreateWindow(width, height, name, nullptr, nullptr);
    assert(pWindow);
    return pWindow;
}

void Vk::DestroyWindow(
    GLFWwindow* pWindow)
{
    glfwDestroyWindow(pWindow);
}

void Vk::ReadFile(
    const char* filename, 
    Vector<u32>& buffer)
{
    buffer.clear();
    FILE* pFile = fopen(filename, "rb");
    assert(pFile);
    fseek(pFile, 0, SEEK_END);
    const size_t bytes = ftell(pFile);
    fseek(pFile, 0, SEEK_SET);
    {
        size_t count = bytes / sizeof(u32);
        if((bytes % sizeof(u32)) > 0u)
        {
            count++;
        }
        buffer.resize((s32)count);
        buffer.back() = 0u;
    }
    fread(buffer.begin(), 1, bytes, pFile);
    fclose(pFile);
}

Vk::Shader Vk::CreateShader(
    VkDevice logicalDevice, 
    Slice<u32> binary, 
    ShaderStage stage)
{
    Shader shader;
    shader.m_stage = stage;

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = binary.count() * sizeof(u32);
    createInfo.pCode = binary.begin();

    assert(vkCreateShaderModule(logicalDevice, &createInfo, nullptr, &shader.m_module) == VK_SUCCESS);

    return shader;
}

void Vk::DestroyShader(
    VkDevice logicalDevice, 
    Vk::Shader shader)
{
    vkDestroyShaderModule(logicalDevice, shader.m_module, nullptr);
}

VkPipelineVertexInputStateCreateInfo Vk::CreateVertexInputInfo()
{
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 0;
    vertexInputInfo.pVertexBindingDescriptions = nullptr;
    vertexInputInfo.vertexAttributeDescriptionCount = 0;
    vertexInputInfo.pVertexAttributeDescriptions = nullptr;
    return vertexInputInfo;
}

VkPipelineInputAssemblyStateCreateInfo Vk::CreateInputAssemblyInfo(
    Topology topo)
{
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = (VkPrimitiveTopology)topo;
    inputAssembly.primitiveRestartEnable = VK_FALSE;
    return inputAssembly;
}

VkViewport Vk::CreateViewport(
    VkExtent2D extent)
{
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float)extent.width;
    viewport.height = (float)extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    return viewport;
}

VkRect2D Vk::CreateScissor(
    s32 x_offset, 
    s32 y_offset, 
    VkExtent2D extent)
{
    VkRect2D scissor = {};
    scissor.offset = { x_offset, y_offset };
    scissor.extent = extent;
    return scissor;
}

VkPipelineViewportStateCreateInfo Vk::CreateViewportStateInfo(
    const VkViewport& viewport, 
    const VkRect2D& scissor)
{
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;
    return viewportState;
}

VkPipelineRasterizationStateCreateInfo Vk::CreateRasterizationInfo(
    PolygonMode polyMode,
    FrontFace frontFace,
    CullMode cullMode)
{
    VkPipelineRasterizationStateCreateInfo rasterInfo = {};
    rasterInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterInfo.depthClampEnable = VK_FALSE;
    rasterInfo.rasterizerDiscardEnable = VK_FALSE;
    rasterInfo.polygonMode = (VkPolygonMode)polyMode;
    rasterInfo.lineWidth = 1.0f;
    rasterInfo.frontFace = (VkFrontFace)frontFace;
    rasterInfo.cullMode = cullMode;
    rasterInfo.depthBiasEnable = VK_FALSE;
    rasterInfo.depthBiasConstantFactor = 0.0f;
    rasterInfo.depthBiasClamp = 0.0f;
    rasterInfo.depthBiasSlopeFactor = 0.0f;
    return rasterInfo;
}

VkPipelineMultisampleStateCreateInfo Vk::CreateMultisampleInfo(
    MultisampleMode mode)
{
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = (VkSampleCountFlagBits)mode;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;
    return multisampling;
}

VkPipelineColorBlendAttachmentState Vk::CreateColorBlendAttachmentInfo(
    bool color, 
    bool blendEnable)
{
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask = 0;
    if(color)
    {
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    }
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
    if(blendEnable)
    {
        colorBlendAttachment.blendEnable = VK_TRUE;
        colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    }
    else
    {
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    }
    return colorBlendAttachment;
}

VkPipelineColorBlendStateCreateInfo Vk::CreateColorBlendStateInfo(
    const VkPipelineColorBlendAttachmentState& colorBlendAttachment)
{
    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;
    return colorBlending;
}

VkPipelineLayout Vk::CreatePipelineLayout(
    VkDevice device,
    Slice<VkDescriptorSetLayout> setLayouts,
    Slice<VkPushConstantRange> pushConstantRanges)
{
    VkPipelineLayout pipelinelayout;

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = (u32)setLayouts.count();
    pipelineLayoutInfo.pSetLayouts = setLayouts.begin();
    pipelineLayoutInfo.pushConstantRangeCount = (u32)pushConstantRanges.count();
    pipelineLayoutInfo.pPushConstantRanges = pushConstantRanges.begin();

    assert(vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelinelayout) == VK_SUCCESS);
    return pipelinelayout;
}

void Vk::DestroyPipelineLayout(
    VkDevice device, 
    VkPipelineLayout layout)
{
    vkDestroyPipelineLayout(device, layout, nullptr);
}

VkAttachmentReference Vk::CreateAttachmentReference(
    VkImageLayout layout, 
    u32 attachment)
{
    VkAttachmentReference reference = {};
    reference.layout = layout;
    reference.attachment = attachment;
    return reference;
}

VkAttachmentDescription Vk::CreateAttachmentDescription(
    const AttachmentOptions& options)
{
    VkAttachmentDescription attachment = {};
    attachment.format = options.m_format;
    attachment.samples = (VkSampleCountFlagBits)options.m_samples;
    attachment.loadOp = (VkAttachmentLoadOp)options.m_loadOp;
    attachment.storeOp = (VkAttachmentStoreOp)options.m_storeOp;
    attachment.stencilLoadOp = (VkAttachmentLoadOp)options.m_stencilLoadOp;
    attachment.stencilStoreOp = (VkAttachmentStoreOp)options.m_stencilStoreOp;
    attachment.initialLayout = (VkImageLayout)options.m_initialLayout;
    attachment.finalLayout = (VkImageLayout)options.m_finalLayout;
    return attachment;
}

VkSubpassDescription Vk::CreateSubpassDescription(
    const SubpassOptions& options)
{
    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = options.m_bindPoint;
    subpass.inputAttachmentCount = (u32)options.m_inputAttachments.count();
    subpass.pInputAttachments = options.m_inputAttachments.begin();
    subpass.colorAttachmentCount = (u32)options.m_colorAttachments.count();
    subpass.pColorAttachments = options.m_colorAttachments.begin();
    subpass.pResolveAttachments = options.m_resolveAttachments.begin();
    subpass.pDepthStencilAttachment = options.m_depthAttachment;
    subpass.preserveAttachmentCount = (u32)options.m_preserveAttachments.count();
    subpass.pPreserveAttachments = options.m_preserveAttachments.begin();
    assert(options.m_resolveAttachments.count() == size_t(0) ||
        options.m_resolveAttachments.count() == options.m_colorAttachments.count());
    return subpass;
}

VkRenderPass Vk::CreateRenderPass(
    VkDevice device,
    Slice<VkAttachmentDescription> attachments,
    Slice<VkSubpassDescription> subpasses,
    Slice<VkSubpassDependency> dependencies)
{
    VkRenderPass renderPass = VK_NULL_HANDLE;

    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = (u32)attachments.count();
    renderPassInfo.pAttachments = attachments.begin();
    renderPassInfo.subpassCount = (u32)subpasses.count();
    renderPassInfo.pSubpasses = subpasses.begin();
    renderPassInfo.dependencyCount = (u32)dependencies.count();
    renderPassInfo.pDependencies = dependencies.begin();

    assert(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) == VK_SUCCESS);
    
    return renderPass;
}

void Vk::DestroyRenderPass(
    VkDevice device, 
    VkRenderPass renderPass)
{
    vkDestroyRenderPass(device, renderPass, nullptr);
}

VkPipeline Vk::CreateGraphicsPipeline(
    VkDevice device, 
    Slice<Shader> shaders,
    const VkPipelineLayout& pipelineLayout,
    VkRenderPass renderPass,
    u32 subpassIdx,
    const GraphicsPipelineOptions& options,
    VkExtent2D extent)
{
    VkPipeline graphicsPipeline;

    Vector<VkPipelineShaderStageCreateInfo> shaderStageCreateInfos;
    for(const Shader& shader : shaders)
    {
        VkPipelineShaderStageCreateInfo sInfo = {};
        sInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        sInfo.stage = (VkShaderStageFlagBits)shader.m_stage;
        sInfo.module = shader.m_module;
        sInfo.pName = "main";
        shaderStageCreateInfos.grow() = sInfo;
    }

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = CreateVertexInputInfo();
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = CreateInputAssemblyInfo(
        options.m_topology);
    VkViewport viewport = CreateViewport(extent);
    VkRect2D scissor = CreateScissor(0, 0, extent);
    VkPipelineViewportStateCreateInfo viewportState = CreateViewportStateInfo(
        viewport, 
        scissor);
    VkPipelineRasterizationStateCreateInfo rasterizer = CreateRasterizationInfo(
        options.m_polygonMode, 
        options.m_frontFace, 
        options.m_cullMode);
    VkPipelineMultisampleStateCreateInfo multisampling = CreateMultisampleInfo(
        options.m_sampleMode);
    // depth and stencil info here...
    VkPipelineColorBlendAttachmentState colorBlendAttachment = CreateColorBlendAttachmentInfo(
        options.m_colorEnabled, 
        options.m_alphaBlend);
    VkPipelineColorBlendStateCreateInfo colorBlending = CreateColorBlendStateInfo(
        colorBlendAttachment);

    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = (u32)shaderStageCreateInfos.count();
    pipelineInfo.pStages = shaderStageCreateInfos.begin();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = nullptr;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr;
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = renderPass;
    pipelineInfo.subpass = subpassIdx;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex = -1;

    assert(vkCreateGraphicsPipelines(
        device, 
        VK_NULL_HANDLE, 
        1, 
        &pipelineInfo, 
        nullptr, 
        &graphicsPipeline
    ) == VK_SUCCESS);

    return graphicsPipeline;
}

void Vk::DestroyGraphicsPipeline(
    VkDevice device,
    VkPipeline pipeline)
{
    vkDestroyPipeline(device, pipeline, nullptr);
}

VkCommandPool Vk::CreateCommandPool(
    VkPhysicalDevice physicalDevice,
    VkDevice logicalDevice,
    u32 flags)
{
    VkCommandPool pool = VK_NULL_HANDLE;
    QueueFamilyIndices qfi = FindQueueFamilies(physicalDevice);
    assert(qfi.m_graphicsFamily != -1);
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = qfi.m_graphicsFamily;
    poolInfo.flags = (VkCommandPoolCreateFlagBits)flags;
    assert(vkCreateCommandPool(logicalDevice, &poolInfo, nullptr, &pool) == VK_SUCCESS);
    return pool;
}

void Vk::DestroyCommandPool(
    VkDevice device,
    VkCommandPool commandPool)
{
    vkDestroyCommandPool(device, commandPool, nullptr);
}

void Vk::AllocateCommandBuffers(
    VkDevice device,
    VkCommandPool commandPool,
    Vector<VkCommandBuffer>& buffers,
    bool indirect)
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    if(indirect)
    {
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    }
    allocInfo.commandBufferCount = (u32)buffers.count();

    assert(vkAllocateCommandBuffers(device, &allocInfo, buffers.begin()) == VK_SUCCESS);
}

void Vk::BeginCommandBuffer(
    VkCommandBuffer buffer)
{
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
    beginInfo.pInheritanceInfo = nullptr;
    VkResult result = vkBeginCommandBuffer(buffer, &beginInfo);
    assert(result == VK_SUCCESS);
}

void Vk::BeginRenderPass(
    VkRenderPass renderPass,
    VkFramebuffer framebuffer,
    VkCommandBuffer cmdBuffer,
    VkExtent2D extent,
    VkClearValue clearColor,
    bool indirect)
{
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = renderPass;
    renderPassInfo.framebuffer = framebuffer;
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = extent;
    renderPassInfo.clearValueCount = 1;
    renderPassInfo.pClearValues = &clearColor;
    vkCmdBeginRenderPass(cmdBuffer, &renderPassInfo, 
        indirect ? 
        VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS : VK_SUBPASS_CONTENTS_INLINE);
}

void Vk::CmdBindGraphicsPipeline(
    VkCommandBuffer cmdBuffer,
    VkPipeline pipeline)
{
    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
}

void Vk::CmdDraw(
    VkCommandBuffer cmdBuffer,
    u32 vertCount,
    u32 instanceCount,
    u32 firstVertex,
    u32 firstInstance)
{
    vkCmdDraw(cmdBuffer, vertCount, instanceCount, firstVertex, firstInstance);
}

void Vk::CmdEndRenderPass(
    VkCommandBuffer cmdBuffer)
{
    vkCmdEndRenderPass(cmdBuffer);
}

void Vk::EndCommandBuffer(
    VkCommandBuffer cmdBuffer)
{
    assert(vkEndCommandBuffer(cmdBuffer) == VK_SUCCESS);
}