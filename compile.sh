#!/bin/bash

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

rm -rf bin
rm -rf shader_bin

if [ "$1" == "clean" ]; then
    rm -rf build
fi

if [ ! -d "./build" ]; then
    mkdir build
    cd build
    cmake .. -G "Visual Studio 15 Win64"
    cd ../
fi

TYPE="Release"

if [ "$1" == "debug" ] || [ "$2" == "debug" ] ; then
    TYPE="Debug"
fi

cmake --build build --config "$TYPE"

if [[ $? > 0 ]]; then
    exit 1
fi

mkdir -p shader_bin
mkdir -p bin/Release
mkdir -p bin/Debug
mkdir -p bin/Release/screenshots
mkdir -p bin/Release/shader_bin
mkdir -p bin/Debug/shader_bin
mkdir -p bin/Release/assets
mkdir -p bin/Debug/assets

pushd shader_src
for filename in ./*; do
    glslangValidator.exe -V ${filename} -o ../shader_bin/${filename}_raw.spv
    spirv-opt -Os ../shader_bin/${filename}_raw.spv -o ../shader_bin/${filename}.spv 
done
popd

cp shader_bin/* bin/Release/shader_bin
cp shader_bin/* bin/Debug/shader_bin

cp lib/*.dll bin/Release
cp lib/*.dll bin/Debug
cp assets/* bin/Release/assets
cp assets/* bin/Debug/assets

echo "Compile script finished."

if [ "$1" == "debug" ] || [ "$2" == "debug" ] ; then
    exit 0
else
    echo "Running executable..."
    pushd bin/Release
    ./main.exe
    popd
fi
